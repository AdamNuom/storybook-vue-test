import Button from './TailwindButton.vue';

// We can specify the control types in argTypes:
export default {
  title: 'Buttons/AdvancedControls',
  component: Button,
  argTypes: {
    color: {
      control: {
        type: 'select',
        options: [
          'red',
          'green',
          'yellow',
          'blue',
          'pink',
          'purple',
          'indigo',
          'gray'
        ]
      }
    },
    shape: { control: { type: 'select', options: ['round', 'square'] } },
    label: {
      control: {
        type: 'text'
      }
    },
    fontColor: {
      control: {
        type: 'color'
      }
    }
  }
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { Button },
  template: '<Button v-bind="$props" />'
});

export const Primary = Template.bind({});

export const BlueButton = Template.bind({});
BlueButton.args = {
  label: 'Blue button!',
  color: 'blue'
};

export const GreenSquareButton = Template.bind({});
GreenSquareButton.args = {
  label: 'A green one!',
  color: 'green',
  shape: 'square'
};
