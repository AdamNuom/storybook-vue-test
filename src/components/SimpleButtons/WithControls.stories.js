import Button from './TailwindButton.vue';

export default {
  title: 'Buttons/SimpleControls',
  // We specify the component and storybook will detect props for us
  component: Button
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { Button },
  template: '<Button v-bind="$props" />'
});

export const Primary = Template.bind({});

export const BlueButton = Template.bind({});
BlueButton.args = {
  label: 'Blue button!',
  color: 'blue'
};

export const GreenSquareButton = Template.bind({});
GreenSquareButton.args = {
  label: 'A green one!',
  color: 'green',
  shape: 'square'
};
