import Button from './TailwindButton.vue';

export default {
  title: 'Buttons/TemplateExamples'
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { Button },
  template: '<Button v-bind="$props" />'
});

export const Default = Template.bind({});

export const BlueButton = Template.bind({});
BlueButton.args = {
  color: 'blue',
  label: 'Blue'
};
