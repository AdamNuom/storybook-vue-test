import Button from './DocumentedButton.vue';

// Add comments to the props in the component or add a description in argTypes to document the usage of a prop.
export default {
  title: 'Buttons/DocumentedButton',
  component: Button,
  argTypes: {
    color: {
      description: 'The background color of the button',
      control: {
        type: 'select',
        options: [
          'red',
          'green',
          'yellow',
          'blue',
          'pink',
          'purple',
          'indigo',
          'gray'
        ]
      }
    },
    shape: {
      description: 'Determines how rounded the corners of the button should be',
      control: { type: 'select', options: ['round', 'square'] }
    },
    label: {
      control: {
        type: 'text'
      }
    },
    fontColor: {
      description: 'Determines the font color. Accepts any CSS colors.',
      control: {
        type: 'color'
      }
    }
  }
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { Button },
  template: '<Button v-bind="$props" />'
});

export const Primary = Template.bind({});

export const BlueButton = Template.bind({});
BlueButton.args = {
  label: 'Blue button!',
  color: 'blue'
};

export const GreenSquareButton = Template.bind({});
GreenSquareButton.args = {
  label: 'A green one!',
  color: 'green',
  shape: 'square'
};
