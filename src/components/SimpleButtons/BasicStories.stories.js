import TailwindButton from './TailwindButton.vue';

export default {
  title: 'Buttons/SimpleExample',
  components: {
    TailwindButton
  }
};

export const ExampleOne = () => ({
  components: {
    TailwindButton
  },
  template: '<TailwindButton />'
});

export const ExampleTwo = () => ({
  components: {
    TailwindButton
  },
  template: '<TailwindButton color="blue" label="Blue" />'
});
